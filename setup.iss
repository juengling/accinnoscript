; Default (example) InnoSetup script to install an Access application
; Author: Christoph J�ngling, https://www.juengling-edv.de
; License: LPGLv3
; Website: https://gitlab.com/juengling/accinnoscript

#define MyAppName "Access-Test-DB"
#define MyAppExeName "Testapplikation.accdb"
#define MyAppVersion "1.0.0"
#define MyAppDate "0000-00-00"
#define MyAppHash "DEADC0DE"
#define MyAppPublisher "Christoph J�ngling"
#define MyAppURL "https://gitlab.com/juengling/accinnoscript"

[Setup]
AppId={#MyAppName}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName} v{#MyAppVersion}
VersionInfoVersion={#MyAppVersion}
VersionInfoTextVersion={#MyAppVersion}
VersionInfoProductVersion={#MyAppVersion}
VersionInfoProductTextVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={userpf}\{#MyAppName}
PrivilegesRequired=lowest
DefaultGroupName={#MyAppName}
OutputBaseFilename={#MyAppName}-setup
Compression=lzma
SolidCompression=yes
DisableWelcomePage=False
ShowLanguageDialog=auto
CloseApplications=True
RestartApplications=False

; Activate code signing settings if you own a certificate and have configured the sign tools
;SignTool=mysign
;SignedUninstaller=True

[Languages]
Name: "EN"; MessagesFile: "compiler:Default.isl"; InfoAfterFile: "info-after-en.rtf"
Name: "DE"; MessagesFile: "compiler:Languages\German.isl"; InfoAfterFile: "info-after-de.rtf"

[Messages]
EN.WelcomeLabel2=This will install [name/ver] (Date {#MyAppDate}, Git #{#MyAppHash}) on your computer.
DE.WelcomeLabel2=Dieser Assistent wird jetzt [name/ver] (Datum {#MyAppDate}, Git #{#MyAppHash}) auf Ihrem Computer installieren.

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Files]
Source: {#MyAppExeName}; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppName}}"; Filename: "{#MyAppURL}"
Name: "{userdesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: shellexec postinstall skipifsilent
