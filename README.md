# Access InnoSetup Script

This is a simple sample InnoSetup script to install an Access database application to the current user's local program folder.

You need the free software [InnoSetup](https://jrsoftware.org/isdl.php) to compile it.

In most cases you may just have to adjust the #define statements at the beginning, and the `SignTool` setting. If you don't use code signing, remove or comment `SignTool` and `SignedUninstaller`.

The example database used in this setup script is not part of this example.
